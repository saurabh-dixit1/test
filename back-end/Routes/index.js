const express = require('express');
const router = express.Router();

router.get('/',(req ,res) => {
   return res.send('Success from Routes');
});

router.use('/transactions' ,require('./transaction'));

module.exports = router;