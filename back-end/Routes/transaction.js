const express = require("express");
const router = express.Router();
// income controller
const {
  AddIncome,
  GetIncome,
  DeleteIncome,
} = require("../Controllers/IncomeController");
// expence controller

const {
  AddExpense,
  GetExpense,
  DeleteExpense,
} = require("../Controllers/ExpenceController");

// income routes
router.post("/add-income", AddIncome);
router.get("/get-incomes", GetIncome);
router.delete("/delete-income/:incomeID", DeleteIncome);

// expence routes
router.post("/add-expense", AddExpense);
router.get("/get-expenses", GetExpense);
router.delete("/delete-expense/:expenseID", DeleteExpense);

module.exports = router;
