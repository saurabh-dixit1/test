
const ExpenseModel = require('../Models/ExpenseModel');


module.exports.AddExpense = async (req, res) => {
  const { title, ammount, date, category, description } = req.body;
  try {
    const income = await ExpenseModel.create({
      title,
      ammount,
      date,
      category,
      description,
    });
    try {
      // validation
      if (!title || !date || !category || !description) {
        return res.status(400).json({
          message: "All Fields are required",
        });
      }
      if (ammount <= 0 || ammount === "number") {
        return res.status(400).json({
          message: "Ammount Should be A positive number",
        });
      }
      await income.save();
      return res.status(200).json({
        message: "Expense Saved Successfully",
        income,
      });
    } catch (error) {
      return res.send(`Error In Saaving In Database : ${error.message}`);
    }
  } catch (error) {
    return res.send(`internal Server Error : ${error.message}`);
  }
};

module.exports.GetExpense = async (req, res) => {
  try {
    const incomes = await ExpenseModel.find().sort({ createdAt: -1 });
    if (incomes.length === 0) {
      return res.status(400).json({
        message: "No Expense Found",
      });
    }
    return res.status(200).json({
      message: "Expense Fetched Successfully",
      incomes,
    });
  } catch (error) {
    return res.send(`internal Server Error : ${error.message}`);
  }
};

module.exports.DeleteExpense = async (req, res) => {
  const { expenseID } = req.params;
  try {
    await ExpenseModel.findByIdAndDelete(expenseID)
      .then((expense) => {
        return res.status(200).json({
          message: "Expense Deleted Successfully",
          expense,
        });
      })
      .catch((error) => {
        return res.send(`Error In deleting Expense Error : ${error.message}`);
      });
  } catch (error) {
    return res.send(`internal Server Error : ${error.message}`);
  }
};


