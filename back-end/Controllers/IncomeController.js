const IncomeModel = require("../Models/IncomeModel");


//  income part
module.exports.AddIncome = async (req, res) => {
  const { title, ammount, date, category, description } = req.body;
  try {
    const income = await IncomeModel.create({
      title,
      ammount,
      date,
      category,
      description,
    });
    try {
      // validation
      if (!title || !date || !category || !description) {
        return res.status(400).json({
          message: "All Fields are required",
        });
      }
      if (ammount <= 0 || ammount === "number") {
        return res.status(400).json({
          message: "Ammount Should be A positive number",
        });
      }
      await income.save();
      return res.status(200).json({
        message: "Income Saved Successfully",
        income,
      });
    } catch (error) {
      return res.send(`Error In Saaving In Database : ${error.message}`);
    }
  } catch (error) {
    return res.send(`internal Server Error : ${error.message}`);
  }
};

module.exports.GetIncome = async (req, res) => {
  try {
    const incomes = await IncomeModel.find().sort({ createdAt: -1 });
    if (incomes.length === 0) {
      return res.status(400).json({
        message: "No Incomes Found",
      });
    }
    return res.status(200).json({
      message: "Income Fetched Successfully",
      incomes,
    });
  } catch (error) {
    return res.send(`internal Server Error : ${error.message}`);
  }
};

module.exports.DeleteIncome = async (req, res) => {
  const { incomeID } = req.params;
  try {
    await IncomeModel.findByIdAndDelete(incomeID)
      .then((income) => {
        return res.status(200).json({
          message: "Income Deleted Successfully",
          income,
        });
      })
      .catch((error) => {
        return res.send(`Error In deleting Income Error : ${error.message}`);
      });
  } catch (error) {
    return res.send(`internal Server Error : ${error.message}`);
  }
};


//  income part end //

