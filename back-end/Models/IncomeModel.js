const mongoose = require('mongoose');
const { Schema , model} = mongoose;

const IncomeSchema = new Schema({
    
    title: {
        type: String,
        require: true,
        trim: true,
        maxLength: 80,
    },

    ammount: {
        type: Number,
        require: true,
        trim: true,
        maxLength: 20,
    },

    type: {
        type: String,
        default: "Income"
    },

    date: {
        type: Date,
        require: true,
        trim: true,
    },

    category: {
        type: String,
        require: true,
        trim: true,
    },

    description: {
        type: String,
        require: true,
        trim: true,
        maxLength: 100,
    }

},{timestamps: true});

module.exports = model('IncomeModel',IncomeSchema);