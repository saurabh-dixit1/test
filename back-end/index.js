const dotenv = require("dotenv");
dotenv.config();
const express = require("express");
const cors = require("cors");
const PORT = process.env.SERVER_PORT;
const app = express();
const DB = require("./Configration/Database");

// middleWares
app.use(express.json());
app.use(cors());

app.use('/',require('./Routes'));

DB.ConnectDb().then(() => {
  app.listen(PORT, () => {
    console.log(`Server Connected Successfull in Port : ${PORT}`);
  });
});
