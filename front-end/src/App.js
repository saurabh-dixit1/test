import { styled } from "styled-components";
import { MainLayout } from "./styles/Layouts";
import Orb from "./components/Orb/Orb";



function App() {
  return (
    <AppStyled className="App">
      <Orb>
      </Orb>
      <MainLayout>
      </MainLayout>
    </AppStyled>
  );
}


const AppStyled = styled.div`
  height: 100vh;
  background-color:white;
  position:relative;
`;
export default App;
