import React from "react";
import { styled } from "styled-components";

function Navigation() {


  return (
    <NavigationStyled>

    </NavigationStyled>
  )
}

const NavigationStyled = styled.div`
    width: 70vh;
    height: 70vh;
    position: absolute;
    margin-left: -37vh;
    margin-top: -37vh;
    background:linear-gradient(180deg,#F56692 0% , #F2994A 100%);
    filter: blur(200px);
`;

export default Navigation;
